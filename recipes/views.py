from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm, EditForm

# Create your views here.
def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = EditForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=recipe.id)
    else:
        form = EditForm(instance=recipe)
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form
    }
    return render(request, "recipes/create.html", context)

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe": recipe
    }

    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render (request, "recipes/list.html", context)
